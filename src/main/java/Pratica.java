
import utfpr.ct.dainf.pratica.PoligonalFechada;
import utfpr.ct.dainf.pratica.PontoXZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica {

    public static void main(String[] args) {
        PoligonalFechada p = new PoligonalFechada();
        PontoXZ p1 = new PontoXZ(0,0);        
        
        p.inserir(new PontoXZ(-3,2));
        p.inserir(new PontoXZ(-3,6));
        p.inserir(new PontoXZ(0,2));
                
        System.out.println("Comprimento da poligonal = "+p.getComprimento());
        
    }
    
 
}
