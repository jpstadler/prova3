package utfpr.ct.dainf.pratica;

import java.util.ArrayList;
import java.util.List;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <T> Tipo do ponto
 */
public class Poligonal<T extends Ponto2D> {
    private final List<T> vertices = new ArrayList<>();

    public void inserir(T p) {
        vertices.add(p);
    }
    
    public int getN() {
        return vertices.size();
    }
    
    public T get(int i) {
        if(i > getN() || i<0)
            throw new RuntimeException("get("+i+"): índice inválido");
        else
            return vertices.get(i);
    }
    
    public void set(int i, T p) {
        if(i<0 || i > getN())
            throw new RuntimeException("set("+i+"): índice inválido");
        else if(i==getN())
            inserir(p);
        else
            set(i, p);
    }
    
    public double getComprimento() {
        double comprimento = 0.0;
        int i;
        
        for(i=0; i<getN()-1; i++)
            comprimento+=get(i).dist(get(i+1));
                       
        return comprimento;
    }
}
